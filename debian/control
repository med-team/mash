Source: mash
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Sascha Steinbiss <satta@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper (>= 13),
               debhelper-compat (= 13),
               capnproto,
               libcapnp-dev,
               libgsl-dev,
               zlib1g-dev,
               libmurmurhash-dev,
               pkg-config
Build-Depends-Indep: python3-sphinx,
                     libjs-mathjax,
                     dh-sequence-sphinxdoc
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/mash
Vcs-Git: https://salsa.debian.org/med-team/mash.git
Homepage: https://mash.readthedocs.io
Rules-Requires-Root: no

Package: mash
Architecture: amd64 arm64 loong64 mips64el ppc64el s390x alpha ppc64 riscv64
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: fast genome and metagenome distance estimation using MinHash
 Mash uses MinHash locality-sensitive hashing to reduce large biosequences to
 a representative sketch and rapidly estimate pairwise distances between
 genomes or metagenomes. Mash sketch databases effectively delineate known
 species boundaries, allow construction of approximate phylogenies, and can be
 searched in seconds using assembled genomes or raw sequencing runs from
 Illumina, Pacific Biosciences, and Oxford Nanopore.
 For metagenomics, Mash scales to thousands of samples and can replicate Human
 Microbiome Project and Global Ocean Survey results in a fraction of the time.

Package: libmash-dev
Architecture: amd64 arm64 loong64 mips64el ppc64el s390x alpha ppc64 riscv64
Section: libdevel
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: development headers and static library for Mash
 This package contains C++ development headers and a static library to
 build custom programs utilizing Mash, a MinHash based genome distance
 estimator.

Package: mash-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends},
         libjs-mathjax
Description: documentation for Mash
 This package contains further documentation (tutorials, explanations,
 etc.) for Mash, a MinHash based genome distance estimator. The documentation
 is provided in HTML format.
